import callApi from './../api/apiCaller'

export const actFetchProductsRequest = () => {
  return callApi('products', 'GET', null).then(res => res.data)
}

export const actDeleteProduct = (id) => {
  return callApi(`products/${id}`, 'DELETE', null).then(res => res)
}

export const actCreateProduct = (data) => {
  return callApi(`products`, 'POST', data).then(res => res)
}

export const actGetProduct = (id) => {
  return callApi(`products/${id}`, 'GET', null).then(res => res.data)
}
