import callApi from './../api/apiCaller'

export const actFetchUsersRequest = () => {
  return callApi('admin/users', 'GET', null).then(res => res.data)
}

export const actDeleteUser = (id) => {
  return callApi(`admin/users/${id}`, 'DELETE', null).then(res => res)
}

export const actCreateUser = (data) => {
  return callApi(`admin/users`, 'POST', data).then(res => res)
}

export const actGetUser = (id) => {
  return callApi(`admin/users/${id}`, 'GET', null)
}

export const actUpdateProduct = (id, data) => {
  return callApi(`admin/users/${id}`, 'PUT', data)
}
