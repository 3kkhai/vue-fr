import Vue from 'vue'
import Router from 'vue-router'
import NProgress from 'nprogress'

Vue.use(Router)

const routes = new Router({
  mode: 'history',
  routes: [
    {
      path: '/admin',
      name: 'Admin',
      component: () => import('@/components/Menu'),
      children: [
        {
          path: 'users',
          name: 'UserIndex',
          component: () => import('@/components/users/UserIndex')
        },
        {
          path: 'users/create',
          name: 'UserCreate',
          component: () => import('@/components/users/UserCreate')
        },
        {
          path: 'edit/:id',
          name: 'UserUpdate',
          component: () => import('@/components/users/UserEdit')
        }
      ]
    }
  ]
})

routes.beforeEach((to, from, next) => {
  NProgress.start()
  next()
})

routes.afterEach(() => {
  NProgress.done()
})

export default routes
