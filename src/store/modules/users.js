import * as types from './../../actions/user'
import * as mutationType from '../mutation_types'
import NProgress from 'nprogress'

const state = {
  all: [],
  user: ''
}
const getters = {
  allUsers: state => {
    return state.all
  },
  findUser: state => {
    return state.user
  }
}
const actions = {
  // get data from API
  async getAllUser ({commit}) {
    commit(mutationType.SHOW_ALL_USER, await types.actFetchUsersRequest())
  },
  async destroy (context, id) {
    if (window.confirm('Are you sure delete this Data?')) {
      NProgress.start()
      await types.actDeleteUser(id)
      context.dispatch('getAllUser')
      NProgress.done()
    }
  },
  async saveUser (context, data) {
    await types.actCreateUser(data)
    context.dispatch('getAllUser')
  },
  async findUser (context, id) {
    try {
      const response = await types.actGetUser(id)
      context.commit(mutationType.FIND_USER, response.data)
      return response
    } catch (e) {
      return e
    }
  },
  async updateUser (context, data) {
    console.log(data)
    await types.actUpdateProduct(data.id, data)
    context.dispatch('getAllUser')
  }
}
const mutations = {
  [mutationType.SHOW_ALL_USER] (state, users) {
    state.all = users
  },
  [mutationType.FIND_USER] (state, user) {
    state.user = user
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
